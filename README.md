# eVisionTech API
## To start:

- Set the absolute path to the config yaml
```YAML
dir_name: 'YOUR_ABSOLUTE_FILE_PATH'
```
- Run docker compose up
- Try GET localhost:8000/api/meta